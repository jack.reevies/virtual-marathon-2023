export function changeColour(node: HTMLObjectElement, accentColour: Record<string, string> | string) {
  if (!node || !node.contentDocument) return

  if (typeof (accentColour) === 'string') {
    return recolourElement(node, 'accent', accentColour)
  }

  for (let i = 0; i < Object.keys(accentColour).length; i++) {
    const key = Object.keys(accentColour)[i]
    const accent = accentColour[key]
    recolourElement(node, key, accent)
  }
}

function recolourElement(node: HTMLObjectElement, key: string, accent: string) {
  if (!node || !node.contentDocument) return
  const accentElements = node.contentDocument!.getElementsByClassName(key)
  Array.from(accentElements).forEach(element => {
    (element as HTMLElement).style.fill = accent
  })
}
