// See https://kit.svelte.dev/docs/types#app
import type { PrismaClient, Route, User, UserRoute } from "@prisma/client"
import type { CurrentUser } from "./lib/server/user";
import type { DiscordUser } from "./routes/login/discord/+server";

// for information about these interfaces
declare global {
	namespace App {
		// interface Error {}
		interface Locals {
			user?: User & {
				distances: Distance[];
				userRoutes: (UserRoute & { route: Route; })[];
				fitbitAuth?: FitbitAuth[];
			}
			auth?: DiscordUser
		}
		// interface PageData {}
		// interface Platform {}
	}
	var prisma: PrismaClient
}

export { };

declare module 'svelte-confetti' {
	
}