import { getImageAsBase64 } from '$lib/server/avatar';
import { getSession } from '$lib/server/database/session';
import { redirect, type Handle, type RequestEvent } from '@sveltejs/kit';
import { loginUser } from './lib/server/user';

async function restoreFromSession(sessionId?: string) {
  if (!sessionId) return

  const session = await getSession(sessionId)
  if (!session) {
    return
  }

  return session.authId
}

async function doLogin(event: RequestEvent) {
  const authId = await restoreFromSession(event.cookies.get('session'));

  if (!authId && !event.url.pathname.startsWith('/login')) {
    redirect(302, '/login');
  }

  if (authId) {
    try {
      event.locals.user = await loginUser(authId);
    } catch (e) {
      event.locals.user = undefined
      const session = crypto.randomUUID()
      event.cookies.set('session', session, { 'httpOnly': false, path: '/', secure: false })
    }
  }
}


export const handle = (async ({ event, resolve }) => {
  if (!event.cookies.get('session')) {
    // Assign empty session
    const session = crypto.randomUUID()
    event.cookies.set('session', session, { 'httpOnly': false, path: '/', secure: false })
  }

  console.log(event.url.pathname)
  const excludeUrls = ['/api/fitbit', '/api/vapidPubKey']

  if (excludeUrls.includes(event.url.pathname)) {
    return await resolve(event);
  }

  if (!event.locals.user) {
    await doLogin(event)
  }

  if (event.locals.user && !event.locals.user.userRoutes.length && event.url.pathname === '/') {
    redirect(302, '/new');
  }

  const response = await resolve(event);
  return response;
}) satisfies Handle;

