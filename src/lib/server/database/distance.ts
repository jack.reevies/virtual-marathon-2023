import { prisma } from "./prisma"

export function getDistancesForUser(userId: string) {
  return prisma.distance.findMany({
    where: { userId }
  })
}

export async function addDistance(userId: string, distanceKM: number, comment: string, userRouteId: string, time?: Date) {
  return await prisma.distance.create({
    data: {
      userId,
      time,
      distanceM: Math.round(distanceKM * 1000),
      comment,
      userRouteId
    }
  })
}

export async function getAllUsersOnRoute(routeId: string, excludeUserId: string) {
  const usersWithRouteAndProperty = await prisma.user.findMany({
    where: {
      userRoutes: {
        some: {
          routeId,
            timeEnded: {
              'isSet': false
            }
        }
      },
      NOT: {
        id: excludeUserId
      },
    },
    select: {
      id: true,
      name: true,
      avatar: true,
      userRoutes: {
        select: {
          distance: {
            select: {
              distanceM: true
            }
          }
        },
        where: {
          routeId
        },
      }
    }
  });

  return usersWithRouteAndProperty.map(o => {
    return {
      id: o.id,
      avatar: o.avatar,
      name: o.name,
      distance: sumDistances(o.userRoutes[0]?.distance)
    }
  })
}

function sumDistances(distances: Array<{ distanceM: number }>) {
  if (!distances) return 0
  return distances.reduce((acc, obj) => acc + obj.distanceM, 0)
}