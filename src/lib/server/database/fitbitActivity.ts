import type { User } from "@prisma/client"
import type { FitbitActivity } from "../fitbit"
import { prisma } from "./prisma"

export async function findActivities(fitbitIds: string[]): Promise<FitbitActivity[]> {
  return await prisma.fitbitActivity.findMany({
    where: { fitbitId: { in: fitbitIds } }
  })
}

export async function getActivities(userId: string) {
  return await prisma.fitbitActivity.findMany({
    where: { userId }
  })
}

export async function getUnconsumedActivities(userId: string) {
  return await prisma.fitbitActivity.findMany({
    where: { userId, consumed: false }
  })
}

export async function recordActivities(userId: string, activities: FitbitActivity[]) {
  let newRecords = []
  for (const o of activities) {

    if (await prisma.fitbitActivity.findUnique({ where: { fitbitId: o.id } })) {
      continue
    }

    newRecords.push(o.id)
    await prisma.fitbitActivity.upsert({
      where: { fitbitId: o.id },
      create: {
        userId: userId,
        fitbitId: o.id,
        type: o.type,
        distance: o.distance,
        steps: o.steps,
        duration: o.duration,
        startTime: o.startTime,
        heartRate: o.heartRate,
        speed: o.speed,
        calories: o.calories
      },
      update: {}
    })
  }

  return newRecords
}

export async function consumeActivities(activities: FitbitActivity[]) {
  return await prisma.fitbitActivity.updateMany({
    where: { fitbitId: { in: activities.map(o => o.fitbitId) } },
    data: { consumed: true }
  })
}