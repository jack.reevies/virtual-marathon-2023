import { prisma } from "./prisma"

export async function connectFitbitWithUser(userId: string, accessToken: string, refreshToken: string, fitbitUserId: string) {
  const user = await prisma.user.findUnique({
    where: { id: userId },
    include: {
      fitbitAuth: true
    }
  })

  if (!user) {
    throw new Error(`User ${userId} not found`)
  }

  if (user.fitbitAuth.length > 0) {
    return await prisma.fitbitAuth.update({
      where: { id: user.fitbitAuth[0].id },
      data: {
        accessToken,
        refreshToken,
        fitbitUserId
      }
    })
  }

  return await prisma.fitbitAuth.create({
    data: {
      userId,
      accessToken,
      refreshToken,
      fitbitUserId
    }
  })
}

export async function getByFitbitUserId(fitbitUserId: string) {
  const user = await prisma.fitbitAuth.findUnique({
    where: { fitbitUserId },
    include: {
      'user': true
    }
  })

  if (!user) {
    throw new Error(`User with fitbitUserId ${fitbitUserId} not found`)
  }

  return user
}