import { prisma } from "./prisma"

export async function createSession(authId: string, userId: string) {
  return await prisma.session.create({
    data: {
      id: crypto.randomUUID(),
      authId,
      userId
    }
  })
}

export async function getSession(sessionid: string) {
  return await prisma.session.findUnique({
    where: { id: sessionid }
  })
}

export async function removeSession(sessionId: string) {
  return await prisma.session.delete({
    where: { id: sessionId }
  })
}