import { prisma } from "./prisma"

export async function getUsersRoutes(userId: string) {
  return await prisma.userRoute.findMany()
}

export async function getUserRoute(userId: string, routeId: string) {
  return await prisma.userRoute.findMany({
    where: {
      userId,
      routeId
    }
  })
}

export async function newUserRoute(routeId: string, userId: string, motivation?: string) {
  return await prisma.userRoute.create({
    data: {
      userId: userId,
      routeId: routeId,
      timeStarted: new Date(),
      timeGoal: new Date(Date.now() + 1000 * 60 * 60 * 24 * 365)
    }
  })
}

export async function finishRoute(userRouteId: string, time: Date = new Date()) {
  return await prisma.userRoute.update({ where: { id: userRouteId }, data: { timeEnded: time } })
}