import { prisma } from "./prisma"

export async function getUser(id: string) {
  return await prisma.user.findUnique({
    where: { id }
  })
}

export async function getUserFromAuthId(authId: string) {
  return await prisma.user.findUniqueOrThrow({
    where: { authId },
    include: {
      distances: true,
      userRoutes: {
        include: {
          distance: true,
          route: true
        }
      }
    }
  })
}

export async function updateUser(id: string, name: string, avatar: string) {
  await prisma.user.update({
    where: { id },
    data: {
      name,
      avatar
    }
  })
}

export async function registerNewUser(authId: string, name: string, avatar: string, accent: string) {
  return await prisma.user.create({
    data: {
      authId,
      name,
      avatar,
      accentColour: accent
    }
  })
}