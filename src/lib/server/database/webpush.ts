import { prisma } from "./prisma"

export async function addSubscription(userId: string, subscription: string) {
  const results = await prisma.webPush.findMany({ where: { subscription } })

  if (results.length > 0) {
    return false
  }

  await prisma.webPush.create({
    data: {
      userId,
      subscription: subscription
    }
  })

  return true
}