import { prisma } from "$lib/server/database/prisma"
import { EMAIL_API_KEY, FIDO_EXPECTED_ORIGIN } from '$env/static/private';

const EMAIL_TEMPLATE = {
  "sender": {
    "name": "Jack Reeve",
    "email": "verify@reevies.tech"
  },
  "to": [
    {
      "email": "jack.reevies@gmail.com",
      "name": "Jack Reeve Recipient"
    }
  ],
  "subject": "Virtual Marathon: Verify email address",
  "htmlContent": "<html><head></head><body><p>Thank you for signing up to (Virtual Marathon)</p>Your verification code is <b>{CODE}</b> <br/> <br/>Alternatively, visit {ORIGIN}/login/verify?code={CODE}<br/><br/><small>Note: This code will expire in 1 hour ({EXPIRES})</small></p></body></html>"
}

export async function generateSecurityCode(userAuthId: string, credentialId: string, publicKey: string) {
  const code = crypto.randomUUID()
  await prisma.emailVerify.create({ data: { code, userAuthId, credentialId, publicKey } })
  await sendVerifyEmail(code, userAuthId)
  return code
}

async function sendVerifyEmail(securityCode: string, emailAddress: string) {
  const email = JSON.parse(JSON.stringify(EMAIL_TEMPLATE))
  email.to[0] = { email: emailAddress, name: emailAddress }
  email.htmlContent = email.htmlContent.replace(/{CODE}/g, securityCode)
  email.htmlContent = email.htmlContent.replace('{EXPIRES}', new Date(Date.now() + 1000 * 60 * 60).toString())
  email.htmlContent = email.htmlContent.replace('{ORIGIN}', FIDO_EXPECTED_ORIGIN)

  const res = await fetch('https://api.sendinblue.com/v3/smtp/email', {
    method: 'POST',
    body: JSON.stringify(email),
    headers: {
      'api-key': EMAIL_API_KEY,
      'content-type': 'application/json'
    }
  })

}

