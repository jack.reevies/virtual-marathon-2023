import { FITBIT_AUTH, FITBIT_ID } from "$env/static/private";
import type { FitbitAuth, User } from "@prisma/client";
import { connectFitbitWithUser } from "./database/fitbitAuth";
import type { FitBitActivityResponse } from "../../types/fitbit";
import { findActivities, recordActivities } from "./database/fitbitActivity";

export interface FitbitActivity {
  id: string
  type: string
  distance: number
  calories: number
  steps: number
  duration: number
  startTime: string
  heartRate: number
  speed: number
  consumed?: boolean
}

export async function getUserActivities(user: User & { fitbitAuth: FitbitAuth[] }): Promise<{ new: string[], activities: FitbitActivity[] }> {
  const json = await makeAuthorisedRequest<FitBitActivityResponse>(user, `https://api.fitbit.com/1/user/-/activities/list.json?beforeDate=${new Date(Date.now() + 1000 * 60 * 60 * 24).toISOString().substring(0, 10)}&limit=100`)

  const activities = json.activities.filter(o => o.distance).map(item => {
    return {
      id: item.logId.toString(),
      type: item.activityName,
      distance: item.distance,
      calories: item.calories,
      steps: item.steps,
      duration: item.duration,
      startTime: item.startTime,
      heartRate: item.averageHeartRate || 0,
      speed: item.speed
    } as FitbitActivity
  })

  const newRecords = await recordActivities(user.id, activities)

  return { new: newRecords, activities: await findActivities(activities.map(o => o.id)) }
}

export async function createSubscription(user: User & { fitbitAuth: FitbitAuth[] }) {
  return await makeAuthorisedRequest(user, `https://api.fitbit.com/1/user/-/activities/apiSubscriptions/${user.fitbitAuth[0].userId}.json`, { method: 'POST' })
}

async function makeAuthorisedRequest<T>(user: User & { fitbitAuth: FitbitAuth[] }, url: string, opts: RequestInit = {}): Promise<T> {
  const res = await fetch(url, {
    ...opts,
    headers: {
      ...(opts.headers || {}),
      "Authorization": `Bearer ${user.fitbitAuth[0].accessToken}`
    }
  })

  if (res.status === 401) {
    await refreshAccessToken(user)
    return await makeAuthorisedRequest(user, url, opts)
  }

  if (res.status > 300) {
    const text = await res.text()
    throw new Error(`Got ${res.status} from Fitbit: ${text}`)
  }

  return await res.json()
}

export async function refreshAccessToken(user: User & { fitbitAuth: FitbitAuth[] }) {
  const tokenResponse = await fetch('https://api.fitbit.com/oauth2/token', {
    method: 'POST',
    headers: {
      'Authorization': `Basic ${FITBIT_AUTH}`,
      "accept": "*/*",
      "accept-language": "en-GB,en;q=0.7",
      "cache-control": "no-cache",
      "content-type": "application/x-www-form-urlencoded",
      "pragma": "no-cache",
      "priority": "u=1, i",
      "sec-ch-ua": "\"Brave\";v=\"129\", \"Not=A?Brand\";v=\"8\", \"Chromium\";v=\"129\"",
      "sec-ch-ua-mobile": "?0",
      "sec-ch-ua-platform": "\"Windows\"",
      "sec-fetch-dest": "empty",
      "sec-fetch-mode": "cors",
      "sec-fetch-site": "same-site",
      "sec-gpc": "1",
      "Referer": "https://dev.fitbit.com/",
      "Referrer-Policy": "strict-origin-when-cross-origin"
    },
    body: `grant_type=refresh_token&refresh_token=${user.fitbitAuth[0].refreshToken}&client_id=${FITBIT_ID}`
  })

  const json = await tokenResponse.json()

  await connectFitbitWithUser(user.id, json.access_token, json.refresh_token, json.user_id)
  user.fitbitAuth[0].accessToken = json.access_token
  user.fitbitAuth[0].refreshToken = json.refresh_token
}
