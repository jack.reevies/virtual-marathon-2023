import { PRIVATE_VAPID } from '$env/static/private';
import { PUBLIC_VAPID } from '$env/static/public';
import webpush from 'web-push';

export async function sendNotificationByUserId(userId: string, payload: string) {
  const user = await prisma.user.findUnique({
    where: { id: userId },
    include: {
      webPush: true
    }
  })

  if (!user) {
    console.error(`User ${userId} not found`)
    throw new Error(`User ${userId} not found`)
  }

  if (!user.webPush.length) {
    console.error(`User ${userId} has no webPush subscriptions`)
    throw new Error(`User ${userId} has no webPush subscriptions`)
  }

  for (const subscription of user.webPush) {
    try { await sendNotification(JSON.parse(subscription.subscription), payload) } catch (e) { console.error(e) }
  }
}

export async function sendNotification(subscription: webpush.PushSubscription, payload: string) {
  webpush.setVapidDetails(
    'mailto:webpush@reevies.tech',
    PUBLIC_VAPID,
    PRIVATE_VAPID
  );

  await webpush.sendNotification(
    subscription,
    payload,
    {
      TTL: 60,
      vapidDetails: {
        subject: 'mailto:webpush@reevies.tech',
        publicKey: PUBLIC_VAPID,
        privateKey: PRIVATE_VAPID
      }
    }
  );
}