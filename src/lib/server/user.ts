import { updateUser } from "./database/users"
import { prisma } from "./database/prisma"

export async function loginUser(authId: string) {
  let dbUser = await prisma.user.findUnique({
    where: { authId },
    include: {
      distances: true,
      fitbitAuth: true,
      userRoutes: {
        where: { timeEnded: { isSet: false } },
        include: {
          route: true
        }
      }
    }
  })

  if (!dbUser) {
    throw new Error('User not found')
  }

  return dbUser
}

export async function update(id: string, name: string, avatar: string) {
  return await updateUser(id, name, avatar)
}