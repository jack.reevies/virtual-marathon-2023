import type { PageServerLoad } from './$types';
import { prisma } from "$lib/server/database/prisma"
import { getAllUsersOnRoute } from '$lib/server/database/distance';

export const load = (async ({ url, locals }) => {
  if (!locals.user?.userRoutes.length) {
    return {
      otherUsers: [],
      distanceM: 0
    }
  }

  const routeId = url.searchParams.get('route')?.toString()
  const userRoute = locals.user.userRoutes.find(o => o.routeId.toString() === routeId && !o.timeEnded) || locals.user.userRoutes.find(o => !o.timeEnded)

  if (!userRoute) {
    return {
      otherUsers: [],
      distanceM: 0,
    }
  }

  const distances = locals.user.distances.filter(o => o.userRouteId === userRoute.id)

  return {
    otherUsers: await getAllUsersOnRoute(userRoute.routeId, locals.user.id) || [],
    userRoute,
    distanceM: distances.reduce((acc, obj) => acc + obj.distanceM, 0),
  }
}) satisfies PageServerLoad;