import { getByFitbitUserId } from '$lib/server/database/fitbitAuth';
import { getUserActivities } from '$lib/server/fitbit';
import { sendNotificationByUserId } from '$lib/server/notification';
import { json, type RequestHandler } from '@sveltejs/kit';

export const GET: RequestHandler = async ({ request }) => {
  const params = new URL(request.url).searchParams;
  const verify = params.get("verify")

  console.log(`Got request to ${request.url}`)

  console.log(`verify is ${verify}`)

  if (verify === '5e242c52961db376373a472785654387274bcc213e70da130f977efa08305f5b') {
    return new Response(null, { status: 204 })
  }

  return new Response(null, { status: 404 })
}

export const POST: RequestHandler = async ({ request }) => {
  const json = await request.json();
  console.log(`Got request to ${request.url} - ${JSON.stringify(json)}`)

  for (const entry of json) {
    console.log(`Got new fitbit activity from ${entry.ownerId} (${JSON.stringify(entry)})`)
    const fitbitAuth = await getByFitbitUserId(entry.ownerId || entry.subscriptionId)

    if (!fitbitAuth) {
      console.error(`Failed to find fitbitAuth for ${entry.ownerId}`)
      return new Response(null, { status: 200 })
    }

    const { new: newRecords, activities } = await getUserActivities({ ...fitbitAuth.user, fitbitAuth: [fitbitAuth] })

    if (!newRecords) {
      return
    }

    // Including test code just to get an id showing
    await sendNotificationByUserId(fitbitAuth.user.id, JSON.stringify({ message: 'Well done on the run! - do you want to log it towards a route?', ids: newRecords }))
  }

  return new Response(null, { status: 200 });
};