import { addSubscription } from '$lib/server/database/webpush';
import { sendNotificationByUserId } from '$lib/server/notification';
import type { RequestHandler } from '@sveltejs/kit'

export const POST: RequestHandler = async ({ request, locals }) => {
  try {
    const subscription = await request.json();

    if (!locals.user) {
      return new Response(
        JSON.stringify({ error: 'User not found' }),
        { status: 401 }
      );
    }

    if (await addSubscription(locals.user.id, JSON.stringify(subscription))) {
      console.log('Subscription stored:', JSON.stringify(subscription));
      await sendNotificationByUserId(locals.user.id, 'Successfully registered for push notifications!');
      console.log(`Sent test notification`)
    }

    return new Response(JSON.stringify({ success: true }));
  } catch (error) {
    return new Response(
      JSON.stringify({ error: 'Failed to register push subscription' }),
      { status: 500 }
    );
  }
};