import { PUBLIC_VAPID } from '$env/static/public';
import { json, type RequestHandler } from '@sveltejs/kit';

export const GET = (() => {
	return json({ data: PUBLIC_VAPID });
}) satisfies RequestHandler;