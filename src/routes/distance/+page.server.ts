import type { Actions } from './$types';
import { addDistance } from '../../lib/server/database/distance';
import type { PageServerLoad } from '../$types';
import { fail, redirect } from '@sveltejs/kit';
import type { Distance, FitbitAuth, Route, User, UserRoute } from '@prisma/client';
import { finishRoute } from '$lib/server/database/userRoute';
import { getRandomEncouragement, getRandomFinishMessage, getRandomItem } from '$lib/server/helpers';
import { prisma } from '$lib/server/database/prisma';
import { noHistoryMessages } from '$lib/server/constants';
import { getUserActivities } from '$lib/server/fitbit';
import { consumeActivities, findActivities, getUnconsumedActivities } from '$lib/server/database/fitbitActivity';

export const load = (async ({ locals, url }) => {
  const searchParams = new URL(url).searchParams;
  return {
    fromSync: await getUnconsumedActivities(locals.user?.id || ''),
    routeOptions: locals.user?.userRoutes.map(o => o.route.name) || [],
    history: await populateRouteNames(locals.user?.distances || []),
    message: getRandomItem(noHistoryMessages),
    distance: searchParams.get('distance') || '',
    tab: searchParams.get('tab') || 1,
    ids: searchParams.get('ids')?.split(',') || []
  }
}) satisfies PageServerLoad;

async function populateRouteNames(distance: Distance[]) {
  if (!distance.length) return []
  const userRoutes = await prisma.userRoute.findMany({ where: { userId: distance[0].userId }, select: { id: true, route: { select: { name: true } } } })
  return distance.map(o => {
    const name = userRoutes.find(x => x.id === o.userRouteId)?.route.name
    return {
      ...o,
      routeName: name,
      iconUrl: `${name?.replace(/ /g, '-').toLowerCase()}.png`
    }
  }).sort((a, b) => a.time > b.time ? -1 : 1)
}

function getIconUrlForRoute(routeName: string) {
  return `${routeName?.replace(/ /g, '-').toLowerCase()}.png`
}


export const actions = {
  fitbit: async ({ locals }) => {
    const activities = (await getUserActivities(locals.user as User & { fitbitAuth: FitbitAuth[] })).activities.filter(o => !o.consumed)

    return {
      types: [...new Set(activities.map(item => item.type))],
      activities: activities
    }
  },
  postFitbit: async (event) => {
    const form = await event.request.formData()
    const ids = (form.get('ids')?.toString() || '').split(',')

    const workouts = await findActivities(ids)

    if (workouts.length !== ids.length) {
      console.log(`Missing some workouts, only found ${workouts.length} of ${ids.length}`)
      return fail(400, { message: 'Some workouts were not found' })
    }

    const routeNames = JSON.parse(form.get('routes')?.toString() || '[]') as string[]

    let finishedRoute: string = ''

    const added = await Promise.allSettled(routeNames.map(async o => {
      if (!event.locals.user) throw new Error(`Did not expect user to be null`)

      const userRoute = event.locals.user.userRoutes.find(x => x.route.name === o) as UserRoute & { route: Route }
      if (!userRoute) return

      await Promise.allSettled(workouts.map(async x => {
        const parsedDate = new Date(x.startTime)
        await addDistance(event.locals.user!.id, x.distance, '', userRoute.id, parsedDate.toString() === "Invalid Date" ? new Date() : parsedDate)

        const distances = event.locals.user!.distances.filter(x => x.userRouteId === userRoute.id)
        const progressM = distances.reduce((acc, obj) => acc + obj.distanceM, 0)

        if (progressM + x.distance * 1000 >= userRoute.route.lengthKM * 1000) {
          // User has finished route
          await finishRoute(userRoute.id)
          finishedRoute = o
        }
      }))

    }))

    await consumeActivities(workouts)

    return {
      message: finishedRoute ? getRandomFinishMessage(finishedRoute) : getRandomEncouragement(),
      confetti: !!finishedRoute
    }
  },
  post: async (event) => {
    if (!event.locals.user) {
      return fail(401, {})
    }

    const form = await event.request.formData()
    const distanceKM = Number(form.get('distance')?.toString())
    const comment = form.get('comment')?.toString() || ''
    const date = form.get('date')?.toString() || ''
    const routeNames = JSON.parse(form.get('routes')?.toString() || '[]') as string[]

    let finishedRoute: string = ''

    const added = await Promise.allSettled(routeNames.map(async o => {
      if (!event.locals.user) throw new Error(`Did not expect user to be null`)

      const userRoute = event.locals.user.userRoutes.find(x => x.route.name === o) as UserRoute & { route: Route }
      if (!userRoute) return
      const parsedDate = new Date(date)
      const distance = await addDistance(event.locals.user.id, distanceKM, comment, userRoute.id, parsedDate.toString() === "Invalid Date" ? new Date() : parsedDate)

      const distances = event.locals.user.distances.filter(x => x.userRouteId === userRoute.id)
      const progressM = distances.reduce((acc, obj) => acc + obj.distanceM, 0)

      if (progressM + distanceKM * 1000 >= userRoute.route.lengthKM * 1000) {
        // User has finished route
        await finishRoute(userRoute.id)
        finishedRoute = o
      }

      return {
        ...distance,
        routeName: userRoute.route.name,
        iconUrl: getIconUrlForRoute(userRoute.route.name)
      }
    }))

    const returnDistances = added.filter(o => (o as any).value).map(o => {
      return {
        ...(o as any).value
      }
    })

    return {
      distances: returnDistances,
      message: finishedRoute ? getRandomFinishMessage(finishedRoute) : getRandomEncouragement(),
      confetti: !!finishedRoute
    }
  }
} satisfies Actions;
