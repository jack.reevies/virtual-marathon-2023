import { connectFitbitWithUser } from '$lib/server/database/fitbitAuth.js';
import { createSubscription } from '$lib/server/fitbit.js';
import { redirect } from '@sveltejs/kit';

export const load = (async (event) => {
  const code = event.url.searchParams.get('code') || ''

  if (!event.locals?.user?.id) {
    console.error(`No user is registered`)
  }

  const res = await fetch("https://api.fitbit.com/oauth2/token", {
    "headers": {
      "accept": "*/*",
      "accept-language": "en-GB,en;q=0.7",
      "authorization": "Basic MjNRNlZZOjIzMWYzNGFkODQzNjNmNDViZGIyYzE2MzU1ODdiM2E5",
      "cache-control": "no-cache",
      "content-type": "application/x-www-form-urlencoded",
      "pragma": "no-cache",
      "priority": "u=1, i",
      "sec-ch-ua": "\"Not A(Brand\";v=\"8\", \"Chromium\";v=\"132\", \"Brave\";v=\"132\"",
      "sec-ch-ua-mobile": "?0",
      "sec-ch-ua-platform": "\"Windows\"",
      "sec-fetch-dest": "empty",
      "sec-fetch-mode": "cors",
      "sec-fetch-site": "same-site",
      "sec-gpc": "1",
      "Referer": "https://dev.fitbit.com/",
      "Referrer-Policy": "strict-origin-when-cross-origin"
    },
    "body": `grant_type=authorization_code&code=${code}&redirect_uri=https%3A%2F%2Fmarathon.reevies.tech%2Ffitbit`,
    "method": "POST"
  });

  if (res.status !== 200) {
    const text = await res.text()
    throw new Error(`Got ${res.status} from Fitbit: ${text}`)
  }

  const { access_token, refresh_token, user_id } = await res.json()

  const resSub = await createSubscription(event.locals.user as any)

  // const { collectionType, ownerId, ownerType, subscriberId, subscriptionId } = await resSub.json()

  console.log(JSON.stringify(resSub))

  await connectFitbitWithUser(event.locals.user!.id, access_token, refresh_token, user_id)
  console.log(`Successfully associated Fitbit with user ${event.locals.user?.id}`)
  redirect(302, '/settings');
})