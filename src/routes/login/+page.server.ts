import { DISCORD_CLIENT_ID, FIDO_EXPECTED_ORIGIN, FIDO_RPID, GOOGLE_CLIENT_ID, OAUTH_HOST } from '$env/static/private';
import { fail, redirect, type Actions } from '@sveltejs/kit';
import type { PageServerLoad } from './$types';
import { generateRegistrationOptions, generateAuthenticationOptions } from '@simplewebauthn/server';
import { prisma } from "$lib/server/database/prisma"
import { isoUint8Array } from '@simplewebauthn/server/helpers';
import { createNewNoncePair, storeNewNoncePair } from '$lib/server/database/noncePair';
import type { U2F, User } from '@prisma/client';

export const load = (async ({ cookies, locals }) => {
  return {
    user: locals.user,
    session: cookies.get('session')
  };
}) satisfies PageServerLoad;

export const actions = {
  discord: async (event) => {
    const url = 'https://discord.com/api/oauth2/authorize?' +
      `client_id=${DISCORD_CLIENT_ID}` +
      `&redirect_uri=${encodeURIComponent(OAUTH_HOST)}%2Flogin%2Fdiscord` +
      '&response_type=code' +
      '&scope=identify%20email' +
      `&state=${event.cookies.get('session') || ''}`

    redirect(302, url);
  },
  google: async (event) => {
    const url =
      'https://accounts.google.com/o/oauth2/v2/auth?' +
      `client_id=${GOOGLE_CLIENT_ID}` +
      '&response_type=code' +
      '&scope=email+profile' +
      `&redirect_uri=${OAUTH_HOST}/login/google` +
      `&state=${event.cookies.get('session') || ''}`

    redirect(302, url);
  },
  fido: async (event) => {
    const form = await event.request.formData()
    const intent = form.get('fido')?.toString()
    const email = form.get('email')?.toString()

    if (!email || !validateEmail(email)) {
      return fail(400, { message: 'Please enter a valid email address' })
    }

    const user = await prisma.user.findUnique({
      where: { authId: email },
      include: {
        auths: true
      }
    })

    const auths = user?.auths.filter(o => o.alias === FIDO_EXPECTED_ORIGIN) || []

    if (!user || !auths.length) {
      return fidoRegister(user, email)
    }

    return fidoAuthenticate(user, email)
  }
} satisfies Actions;

function validateEmail(email: string) {
  return email.toUpperCase().match(/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/)
}

async function fidoRegister(user: (User & { auths: U2F[] }) | null, email: string) {
  const opts = generateRegistrationOptions({
    rpName: 'Virtual Marathon App',
    rpID: FIDO_RPID,
    userID: email,
    userName: email,
    attestationType: 'none',
    excludeCredentials: user?.auths.map(o => {
      return {
        id: isoUint8Array.fromHex(o.credentialId),
        type: 'public-key'
      }
    }) || [],
  })

  const challenge = opts.challenge
  await storeNewNoncePair(challenge, email)

  return {
    url: `${FIDO_EXPECTED_ORIGIN}/login/fido`,
    intent: 'register',
    opts
  }
}

async function fidoAuthenticate(user: (User & { auths: U2F[] }) | null, email: string) {
  if (!user || !user.auths.length) {
    return fail(404, { message: 'user not found or has no FIDO registration' })
  }

  const opts = generateAuthenticationOptions({
    allowCredentials: user.auths.map(authenticator => {
      return {
        id: isoUint8Array.fromHex(authenticator.credentialId),
        type: 'public-key',
      }
    }),
    userVerification: 'preferred',
  })

  const challenge = opts.challenge
  await storeNewNoncePair(challenge, email)

  return {
    challenge: challenge,
    url: `${FIDO_EXPECTED_ORIGIN}/login/fido`,
    intent: 'login', opts
  }
}