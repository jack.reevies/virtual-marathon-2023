import type { RequestHandler } from './$types';
import { DISCORD_CLIENT_SECRET, DISCORD_CLIENT_ID, OAUTH_HOST } from '$env/static/private';
import { fail, redirect, type RequestEvent } from '@sveltejs/kit';
import { update } from '$lib/server/user';
import { createSession } from '$lib/server/database/session';
import { getUserFromAuthId, registerNewUser } from '$lib/server/database/users';
import { discordColourToHex } from '../../../helpers';
import { getRandomAvatar } from '$lib/server/avatar';

export const GET: RequestHandler = async (event) => {
  const code = event.url.searchParams.get('code')
  const state = event.url.searchParams.get('state')

  if (state !== event.cookies.get('session')) {
    redirect(302, '/login?error=State+did+not+match');
  }

  if (!code) {
    const error = event.url.searchParams.get('error')
    if (error) {
      redirect(302, `/login?error=Discord+login+was+cancelled`);
    }

    throw fail(500, { error: 'Discord login failed' })
  }

  const user = await authoriseUser(code)

  if (!user.email) {
    redirect(302, `/login?error=Discord+login+was+cancelled`); 
  }

  event.locals.auth = user
  try {
    const localUser = await getUserFromAuthId(user.email)
    if (localUser) {
      await update(localUser.id, user.username, await getAvatarUrl(user.id, user.avatar))
    }
    event.locals.user = await getUserFromAuthId(user.email)
  } catch (e) {
    // Maybe register user?
    if (!user.email) {
      throw new Error(`User not found and cannot be registered`)
    }

    await registerNewUser(user.email, user.username, await getAvatarUrl(user.id, user.avatar), discordColourToHex(user.accent_color))
    event.locals.user = await getUserFromAuthId(user.email)
  }
  const session = await createSession(event.locals.user.authId, event.locals.user.id)
  event.cookies.set('session', session.id, { 'httpOnly': false, path: '/', secure: false })

  if (!event.locals.user.userRoutes.length) {
    redirect(302, '/new');
  }
  redirect(302, '/');
};

async function getAvatarUrl(discordId: string, avatar: string) {
  if (!avatar) {
    return await getRandomAvatar(discordId)
  }
  return `https://cdn.discordapp.com/avatars/${discordId}/${avatar}.png`
}

async function authoriseUser(code: string) {
  const res = await fetch(`https://discord.com/api/oauth2/token`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    body: new URLSearchParams({
      client_id: DISCORD_CLIENT_ID,
      client_secret: DISCORD_CLIENT_SECRET,
      grant_type: 'authorization_code',
      code: code,
      redirect_uri: `${OAUTH_HOST}/login/discord`,
    })
  })
  const json = await res.json() as DiscordAuthToken
  return await getUserProfile(json)
}

type DiscordAuthToken = {
  access_token: string
  token_type: string
  expires_in: number
  refresh_token: string
  scope: string
}

export type DiscordUser = {
  id: string
  username: string
  discriminator: string
  avatar: string
  accent_color?: number
  locale?: string
  email?: string
}

async function getUserProfile(token: DiscordAuthToken) {
  const res = await fetch('https://discord.com/api/users/@me', {
    headers: {
      Authorization: `Bearer ${token.access_token}`
    }
  })

  const json = await res.json() as DiscordUser
  return json
}