import type { RequestEvent, RequestHandler } from './$types';
import { FIDO_EXPECTED_ORIGIN, FIDO_RPID } from '$env/static/private';
import { json as responseJson } from '@sveltejs/kit';
import { getUserFromAuthId } from '$lib/server/database/users';
import { createSession } from '$lib/server/database/session';
import { verifyAuthenticationResponse, verifyRegistrationResponse } from '@simplewebauthn/server';
import { prisma } from "$lib/server/database/prisma"
import base64url from 'base64url'
import { isoUint8Array } from '@simplewebauthn/server/helpers'
import { findNonceFromData } from '$lib/server/database/noncePair';
import { generateSecurityCode } from '$lib/server/email';

export const POST: RequestHandler = async (event) => {
  const json = await event.request.json()
  const challenge = await findNonceFromData(json.username)

  if (json.intent === 'register') {
    return await fidoRegister(json, challenge.nonce)
  }

  // Else if login
  const user = await prisma.user.findUnique({ where: { authId: json.username }, include: { auths: true } })
  const bodyCredIDBuffer = base64url.toBuffer(json.rawId);
  const auth = user?.auths.find(o => o.alias === FIDO_EXPECTED_ORIGIN && isoUint8Array.areEqual(isoUint8Array.fromHex(o.credentialId), bodyCredIDBuffer))

  if (!user || !auth) {
    return responseJson({ success: false, message: 'User not found or not setup for FIDO on this device' }, { status: 404 })
  }

  try {
    await verifyAuthenticationResponse({
      response: json,
      expectedChallenge: challenge.nonce,
      expectedOrigin: FIDO_EXPECTED_ORIGIN,
      expectedRPID: FIDO_RPID,
      authenticator: {
        credentialID: isoUint8Array.fromHex(auth.credentialId),
        credentialPublicKey: isoUint8Array.fromHex(auth.publicKey)
      }
    })

    await loginUser(event, json.username)
    return responseJson({ success: true }, { status: 200 })
  } catch (e) {
    console.log(e)
    return responseJson({ success: false, message: 'Login rejected' }, { status: 500 })
  }

};

async function fidoRegister(json: any, challenge: string) {
  try {
    const reg = await verifyRegistrationResponse({
      response: json,
      expectedChallenge: challenge,
      expectedOrigin: FIDO_EXPECTED_ORIGIN,
      expectedRPID: FIDO_RPID
    })

    if (!reg.registrationInfo) {
      throw new Error(`FIDO verification failed`)
    }

    const { credentialID, credentialPublicKey } = reg.registrationInfo

    await generateSecurityCode(json.username, isoUint8Array.toHex(credentialID), isoUint8Array.toHex(credentialPublicKey))

    return responseJson({ success: true }, { status: 201 })
  } catch (e: any) {
    console.log(e)
    return responseJson({ success: false, message: e.message }, { status: 400 })
  }
}

async function loginUser(event: RequestEvent, authId: string) {
  event.locals.user = await getUserFromAuthId(authId)
  const session = await createSession(authId, event.locals.user.id)
  event.cookies.set('session', session.id, { 'httpOnly': false, path: '/', secure: false })
}