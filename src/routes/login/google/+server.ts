import { GOOGLE_CLIENT_ID, GOOGLE_CLIENT_SECRET, OAUTH_HOST } from '$env/static/private';
import { createSession } from '$lib/server/database/session';
import { getUserFromAuthId, registerNewUser } from '$lib/server/database/users';
import { update } from '$lib/server/user';
import { fail, redirect } from '@sveltejs/kit';
import type { RequestHandler } from './$types';

export const GET: RequestHandler = async (event) => {
  const code = event.url.searchParams.get('code')
  const state = event.url.searchParams.get('state')

  if (state !== event.cookies.get('session')) {
    redirect(302, '/login?error=State+did+not+match');
  }

  if (!code) {
    const error = event.url.searchParams.get('error')
    if (error) {
      redirect(302, `/login?error=Google+login+was+cancelled`);
    }

    throw fail(500, { error: 'Google login failed' })
  }

  const token = await authoriseUser(code)
  const user = await getUserProfile(token)

  try {
    const localUser = await getUserFromAuthId(user.email)
    if (localUser) {
      await update(localUser.id, user.username, getAvatarUrl(user.avatar))
    }
    event.locals.user = await getUserFromAuthId(user.email)
  } catch (e) {
    // Maybe register user?
    if (!user.email) {
      throw new Error(`User not found and cannot be registered`)
    }

    await registerNewUser(user.email, user.username, getAvatarUrl(user.avatar), '#d891ff')
    event.locals.user = await getUserFromAuthId(user.email)
  }
  const session = await createSession(event.locals.user.authId, event.locals.user.id)
  event.cookies.set('session', session.id, { 'httpOnly': false, path: '/', secure: false })

  if (!event.locals.user.userRoutes.length) {
    redirect(302, '/new');
  }
  redirect(302, '/');
};

async function authoriseUser(code: string) {
  const response = await fetch('https://oauth2.googleapis.com/token', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    body: new URLSearchParams({
      code,
      client_id: GOOGLE_CLIENT_ID,
      client_secret: GOOGLE_CLIENT_SECRET,
      redirect_uri: `${OAUTH_HOST}/login/google`,
      grant_type: 'authorization_code',
    })
  })

  const json = await response.json()
  const token = json.access_token

  if (!token) {
    throw new Error('Token not received')
  }

  return token
}

async function getUserProfile(token: string) {
  const res = await fetch('https://www.googleapis.com/oauth2/v1/userinfo', {
    headers: {
      Authorization: `Bearer ${token}`,
    }
  })

  const json = await res.json();

  const { email, name, picture, given_name, family_name } = json
  if (!email) {
    throw new Error('Failed to get user\'s Google profile')
  }

  let username = name

  if (given_name) {
    username = given_name
  }

  if (family_name) {
    username = `${username} ${family_name}`
  }

  return {
    email,
    username,
    avatar: picture
  }
}

function getAvatarUrl(avatar: string) {
  if (!avatar) {
    return `https://www.pngkey.com/png/full/115-1150152_default-profile-picture-avatar-png-green.png`
  }
  return avatar
}