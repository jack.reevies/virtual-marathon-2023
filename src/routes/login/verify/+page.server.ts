import { FIDO_EXPECTED_ORIGIN } from '$env/static/private'
import { getRandomAvatar } from '$lib/server/avatar'
import { getUserFromAuthId, registerNewUser } from '$lib/server/database/users'
import { generateUsername } from '$lib/server/helpers'
import { DEFAULT_COLOUR } from '../../../helpers'
import { prisma } from "$lib/server/database/prisma"
import type { PageServerLoad, RequestEvent } from '../$types'
import { createSession } from '$lib/server/database/session'

export const load = (async (event) => {
  const code = event.url.searchParams.get('code') || ''
  const emailVerify = await prisma.emailVerify.findUnique({ where: { code } })

  if (code.startsWith('test')) {
    return {
      user: {
        accentColour: DEFAULT_COLOUR
      },
      isNewUser: code === 'testNew'
    }
  }

  if (!emailVerify) {
    return {
      message: 'This email link has expired - please try signing in again!'
    }
  }

  await prisma.emailVerify.delete({ where: { code } })

  const { userAuthId, credentialId, publicKey } = emailVerify
  const tor = await associateFidoWithUser(userAuthId, credentialId, publicKey)

  await loginUser(event, tor.user.authId)

  return tor

}) satisfies PageServerLoad;

async function associateFidoWithUser(userAuthId: string, credentialId: string, publicKey: string) {
  let user = await prisma.user.findUnique({
    where: { authId: userAuthId }
  })

  let isNewUser = false

  if (!user) {
    // We need to register this user first
    const tempAvatar = await getRandomAvatar(userAuthId)
    user = await registerNewUser(userAuthId, generateUsername(15, 0), tempAvatar, DEFAULT_COLOUR)
    isNewUser = true
  }

  await prisma.u2F.create({
    data: {
      alias: FIDO_EXPECTED_ORIGIN,
      credentialId: credentialId,
      publicKey: publicKey,
      userId: user.id
    }
  })

  return { user, isNewUser }
}

async function loginUser(event: RequestEvent, authId: string) {
  event.locals.user = await getUserFromAuthId(authId)
  const session = await createSession(authId, event.locals.user.id)
  event.cookies.set('session', session.id, { 'httpOnly': false, path: '/', secure: false })
}