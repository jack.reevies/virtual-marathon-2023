import { redirect } from '@sveltejs/kit';
import type { RequestHandler } from './$types';

export const GET: RequestHandler = async (event) => {
    return POST(event)
};

export const POST: RequestHandler = async ({ cookies }) => {
    cookies.set('session', '', { 'expires': new Date(1), 'path': '/', secure: false })
    redirect(302, '/login');
};