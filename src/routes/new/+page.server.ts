import { getByName } from '$lib/server/database/route';
import { newUserRoute } from '$lib/server/database/userRoute';
import { redirect, type Actions } from '@sveltejs/kit';
import type { PageServerLoad } from './$types';

export const load = (async ({ locals }) => {
    return {
        englishChannel: await getByName("English Channel"),
        uk: await getByName("Length of the UK"),
        user: locals.user
    };
}) satisfies PageServerLoad;

export const actions = {
    test: async (event) => {
        // Sign user up for a new route
        // First make sure they arent already signed up to this route
        const form = await event.request.formData()
        const routeId = form.get('card')?.toString()
        const userId = event.locals.user?.id

        if (!routeId || !userId) {
            redirect(302, '/new');
        }

        const existing = event.locals.user?.userRoutes.find(o => o.routeId === routeId && !o.timeEnded)

        if (existing) {
            // They're already on this route - silently redirect them to their current route
            redirect(302, `/?route=${routeId}`);
        }

        newUserRoute(routeId, userId, 'This is my first challenge!')
        console.log(`Embarking ${userId} on a new adventure through ${routeId}`)
        redirect(302, `/?route=${routeId}`);
    }
} satisfies Actions;