import { FIDO_EXPECTED_ORIGIN, FIDO_RPID, FITBIT_ID } from '$env/static/private';
import { storeNewNoncePair } from '$lib/server/database/noncePair';
import { ensureDummyRoutes, prisma } from '$lib/server/database/prisma';
import type { U2F, User } from '@prisma/client';
import { generateRegistrationOptions } from '@simplewebauthn/server';
import { isoUint8Array } from '@simplewebauthn/server/helpers';
import { fail, redirect, type Actions } from '@sveltejs/kit';
import type { PageServerLoad } from './$types';

export const load = (async (event) => {
  return {
    user: event.locals.user
  };
}) satisfies PageServerLoad;

export const actions: Actions = {
  dummyRoutes: async ({ locals }) => {
    await ensureDummyRoutes()

    if (!locals.user) {
      return console.log(`no user`)
    }

    const routes = await prisma.route.findMany()
    Promise.all(routes.map(o => {
      if (locals.user?.userRoutes.find(o => o.routeId === o.routeId)) return Promise.resolve()
      createDummyRouteForUser(locals.user?.id!, o.id)
    }))
  },
  logout: async ({ cookies }) => {
    cookies.set('session', '', { 'expires': new Date(1), 'path': '/', secure: false })
    redirect(302, '/login');
  },
  delete: async ({ locals }) => {
    if (!locals.user) {
      return console.log(`no user`)
    }

    await prisma.distance.deleteMany({
      where: { userId: locals.user.id }
    })

    await prisma.userRoute.deleteMany({
      where: { userId: locals.user.id }
    })
  },
  update: async ({ locals, request }) => {
    const { user } = locals
    const form = await request.formData()
    const username = form.get('Username')?.toString()
    const accent = form.get('Theme Colour')?.toString()

    if (!user || !username || !accent) {
      return
    }

    const updated = await prisma.user.update({
      where: { authId: user.authId },
      data: {
        name: username,
        accentColour: accent
      }
    })

    return { name: updated.name, accentColour: updated.accentColour }
  },
  fido: async ({ locals }) => {
    if (!locals.user) {
      throw fail(500)
    }

    const auths = await prisma.u2F.findMany({ where: { userId: locals.user.id } })

    const opts = generateRegistrationOptions({
      rpName: 'Virtual Marathon App',
      rpID: FIDO_RPID,
      userID: locals.user.authId,
      userName: locals.user.authId,
      attestationType: 'none',
      excludeCredentials: auths.map(o => {
        return {
          id: isoUint8Array.fromHex(o.credentialId),
          type: 'public-key'
        }
      }) || [],
    })

    const challenge = opts.challenge
    await storeNewNoncePair(challenge, locals.user.authId)

    return {
      url: `${FIDO_EXPECTED_ORIGIN}/settings`,
      intent: 'register',
      opts
    }
  },
  fitbit: async ({ locals }) => {
    const authUrl = `https://www.fitbit.com/oauth2/authorize?response_type=code&client_id=${FITBIT_ID}&scope=activity&redirect_uri=https%3A%2F%2Fmarathon.reevies.tech%2Ffitbit`
    redirect(302, authUrl);
  }
};

async function createDummyRouteForUser(userId: string, routeId: string) {
  return prisma.userRoute.create({
    data: {
      userId: userId,
      routeId: routeId,
      timeStarted: new Date(),
      timeGoal: new Date("2024-01-01")
    }
  })
}