import { FIDO_EXPECTED_ORIGIN, FIDO_RPID } from '$env/static/private';
import { findNonceFromData } from '$lib/server/database/noncePair';
import { verifyRegistrationResponse } from '@simplewebauthn/server';
import type { RequestHandler } from './$types';
import { json as responseJson } from '@sveltejs/kit';
import { isoUint8Array } from '@simplewebauthn/server/helpers';
import { prisma } from "$lib/server/database/prisma"

export const POST: RequestHandler = async (event) => {
  if (!event.locals.user) {
    return responseJson({ message: 'No user is currently logged in' }, { status: 401 })
  }

  const json = await event.request.json()
  const challenge = await findNonceFromData(json.username)

  try {
    const reg = await verifyRegistrationResponse({
      response: json,
      expectedChallenge: challenge.nonce,
      expectedOrigin: FIDO_EXPECTED_ORIGIN,
      expectedRPID: FIDO_RPID
    })

    await prisma.u2F.create({
      data: {
        alias: FIDO_EXPECTED_ORIGIN,
        credentialId: isoUint8Array.toHex(reg.registrationInfo!.credentialID),
        publicKey: isoUint8Array.toHex(reg.registrationInfo!.credentialPublicKey),
        userId: event.locals.user.id
      }
    })

    return responseJson({ success: true }, { status: 200 })
  } catch (e: any) {
    console.log(e)
    return responseJson({ success: false, message: e.message }, { status: 400 })
  }
};