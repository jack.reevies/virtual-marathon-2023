// self.addEventListener('notificationclick', async (event: any) => {
//   event.notification.close();

//   const payload = event.data?.text() ?? `{ message: 'no payload' }`;
//   console.log(payload)
//   const json = JSON.parse(payload)

//   // Focus the main window if it exists, otherwise open a new one
//   if (clients.openWindow) {
//     await clients.openWindow(`/distance?tab=2&ids=${json.ids}`);
//   }
// });

self.addEventListener('fetch', function () {
  return;
});

// eslint-disable-next-line @typescript-eslint/no-explicit-any
self.addEventListener('push', function (event: any) {
  const payload = event.data?.text() ?? `{ message: 'no payload' }`;
  console.log(payload)
  const json = JSON.parse(payload)
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  const registration = (self as any).registration as ServiceWorkerRegistration;
  event.waitUntil(registration.showNotification('Virtual Marathon', { body: json.message, data: payload, tag: json.ids.join(','), actions: [{ action: 'update', title: 'Log Progress' }] }));
} as EventListener);

self.addEventListener('notificationclick', async (event: any) => {
  // Get the notification payload
  console.log(JSON.stringify(event.notification.data))
  console.log(JSON.stringify(event.notification))
  console.log(JSON.stringify(event))
  const payload = event.notification.data;

  // Parse the JSON payload if it exists
  let ids = [];
  if (payload && typeof payload === 'string') {
    try {
      const parsedPayload = JSON.parse(payload);
      // Assuming payload contains an array of IDs
      ids = Array.isArray(parsedPayload.ids) ? parsedPayload.ids : [];
    } catch (error) {
      console.error('Error parsing notification payload:', error);
    }
  }

  const baseUrl = `/distance?tab=2&ids=${ids.join(',')}`;

  console.log(`want to open to ${baseUrl}`)

  await clients.openWindow(baseUrl);
});