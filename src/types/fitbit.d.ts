export interface FitBitActivityResponse {
  pagination: Pagination;
  activities: Activity[];
}

export interface Activity {
  logId: number;
  activityTypeId: number;
  activityName: string;
  calories: number;
  distance?: number;
  steps: number;
  speed?: number;
  pace?: number;
  averageHeartRate: number;
  duration: number;
  activeDuration: number;
  activityLevel: ActivityLevel[];
  distanceUnit?: string;
  source?: Source;
  logType: string;
  manualValuesSpecified: ManualValuesSpecified;
  intervalWorkoutData: IntervalWorkoutData;
  heartRateZones: HeartRateZone[];
  activeZoneMinutes: ActiveZoneMinutes;
  inProgress: boolean;
  caloriesLink: string;
  heartRateLink: string;
  tcxLink: string;
  lastModified: string;
  startTime: string;
  originalStartTime: string;
  originalDuration: number;
  elevationGain: number;
  hasActiveZoneMinutes: boolean;
  hasGps?: boolean;
}

export interface ActiveZoneMinutes {
  totalMinutes: number;
  minutesInHeartRateZones: MinutesInHeartRateZone[];
}

export interface MinutesInHeartRateZone {
  minutes: number;
  zoneName: string;
  order: number;
  type: string;
  minuteMultiplier: number;
}

export interface HeartRateZone {
  minutes: number;
  caloriesOut: number;
  name: string;
  min: number;
  max: number;
}

export interface IntervalWorkoutData {
  intervalSummaries: any[];
  numRepeats: number;
}

export interface ManualValuesSpecified {
  calories: boolean;
  distance: boolean;
  steps: boolean;
}

export interface Source {
  type: string;
  name: string;
  id: string;
  url: string;
  trackerFeatures: string[];
}

export interface ActivityLevel {
  minutes: number;
  name: string;
}

export interface Pagination {
  beforeDate: string;
  limit: number;
  next: string;
  offset: number;
  previous: string;
  sort: string;
}