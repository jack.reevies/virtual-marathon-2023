import { sveltekit } from '@sveltejs/kit/vite';
import type { UserConfig } from 'vite';

const config: UserConfig = {
	plugins: [sveltekit()],
	server: {
		allowedHosts: ['localpc.com', 'localhost', '127.0.0.1', '.reevies.tech', '.local']
	}
};

export default config;
